package com.example.movieproject.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class Movie implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="mov_id")
    private Long movieId;
	
	@Column(name="mov_title")
    private String movieTitle;
    
	@Column(name="mov_year", insertable = false, updatable =false)
    private int movieYear;
	
	@Column(name="mov_time")
    private int movieTime;
	
	@Column(name="mov_lang")
    private String movieLanguage;
	
	@Temporal(TemporalType.TIMESTAMP)
	@CreatedDate
    @JsonFormat(pattern = "yyyy-MM-dd")
	@Column(name="mov_dt_rel")
    private Date movieDateRelease;
	
	@Column(name="mov_rel_country")
    private String movieReleaseCountry;
	
	@OneToMany(mappedBy = "movie", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<MovieCast> movieCasts;
	
	@OneToMany(mappedBy = "movie", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<MovieDirection> movieDirections;
	
	@OneToMany(mappedBy = "movie", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<MovieGenre> movieGenres;
	
	@OneToMany(mappedBy = "movie", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<Rating> ratings;
	
	public Movie() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Movie(Long movieId, String movieTitle, int movieYear, int movieTime, String movieLanguage,
			Date movieDateRelease, String movieReleaseCountry) {
		super();
		this.movieId = movieId;
		this.movieTitle = movieTitle;
		this.movieYear = movieYear;
		this.movieTime = movieTime;
		this.movieLanguage = movieLanguage;
		this.movieDateRelease = movieDateRelease;
		this.movieReleaseCountry = movieReleaseCountry;
	}

	public Long getMovieId() {
		return movieId;
	}

	public String getMovieTitle() {
		return movieTitle;
	}

	public int getMovieYear() {
		return movieYear;
	}

	public int getMovieTime() {
		return movieTime;
	}

	public String getMovieLanguage() {
		return movieLanguage;
	}

	public Date getMovieDateRelease() {
		return movieDateRelease;
	}

	public String getMovieReleaseCountry() {
		return movieReleaseCountry;
	}	

	public void setMovieId(Long movieId) {
		this.movieId = movieId;
	}

	public void setMovieTitle(String movieTitle) {
		this.movieTitle = movieTitle;
	}

	public void setMovieYear(int movieYear) {
		this.movieYear = movieYear;
	}

	public void setMovieTime(int movieTime) {
		this.movieTime = movieTime;
	}

	public void setMovieLanguage(String movieLanguage) {
		this.movieLanguage = movieLanguage;
	}

	public void setMovieDateRelease(Date movieDateRelease) {
		this.movieDateRelease = movieDateRelease;
	}

	public void setMovieReleaseCountry(String movieReleaseCountry) {
		this.movieReleaseCountry = movieReleaseCountry;
	}
}
