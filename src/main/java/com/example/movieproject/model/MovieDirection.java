package com.example.movieproject.model;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name = "movie_direction")
public class MovieDirection implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	MovieDirectionKey id;
	
	@MapsId("dir_id")
	@ManyToOne
	@JoinColumn(name = "dir_id")
	private Director director;

	@MapsId("mov_id")
	@ManyToOne
	@JoinColumn(name = "mov_id")
	private Movie movie;

	public MovieDirection() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MovieDirection(Director director, Movie movie) {
		super();
		this.director = director;
		this.movie = movie;
	}

	public MovieDirectionKey getId() {
		return id;
	}

	public Director getDirector() {
		return director;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setId(MovieDirectionKey id) {
		this.id = id;
	}

	public void setDirector(Director director) {
		this.director = director;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MovieDirection)) return false;
        MovieDirection that = (MovieDirection) o;
		return Objects.equals(movie.getMovieId(), that.movie.getMovieId()) &&
                Objects.equals(director.getDirectorId(), that.director.getDirectorId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(movie.getMovieId(), director.getDirectorId());
    }
}
