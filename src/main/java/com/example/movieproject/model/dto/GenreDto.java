package com.example.movieproject.model.dto;

import java.io.Serializable;

public class GenreDto implements Serializable{

	private static final long serialVersionUID = 1L;
	private Long genreId;
	private String genreTitle;
	
	public Long getGenreId() {
		return genreId;
	}
	public String getGenreTitle() {
		return genreTitle;
	}
	public void setGenreId(Long genreId) {
		this.genreId = genreId;
	}
	public void setGenreTitle(String genreTitle) {
		this.genreTitle = genreTitle;
	}
	public GenreDto(Long genreId, String genreTitle) {
		super();
		this.genreId = genreId;
		this.genreTitle = genreTitle;
	}
	public GenreDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
