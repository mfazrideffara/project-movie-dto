package com.example.movieproject.model.dto;

import java.io.Serializable;
import java.util.Date;

public class MovieDto implements Serializable{

	private static final long serialVersionUID = 1L;
	private Long movieId;
	private String movieTitle;
	private int movieYear;
	private int movieTime;
	private String movieLanguage;
	private Date movieDateRelease;
	private String movieReleaseCountry;
	
	public MovieDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	public MovieDto(Long movieId, String movieTitle, int movieYear, int movieTime, String movieLanguage,
			Date movieDateRelease, String movieReleaseCountry) {
		super();
		this.movieId = movieId;
		this.movieTitle = movieTitle;
		this.movieYear = movieYear;
		this.movieTime = movieTime;
		this.movieLanguage = movieLanguage;
		this.movieDateRelease = movieDateRelease;
		this.movieReleaseCountry = movieReleaseCountry;
	}
	public Long getMovieId() {
		return movieId;
	}
	public String getMovieTitle() {
		return movieTitle;
	}
	public int getMovieYear() {
		return movieYear;
	}
	public int getMovieTime() {
		return movieTime;
	}
	public String getMovieLanguage() {
		return movieLanguage;
	}
	public Date getMovieDateRelease() {
		return movieDateRelease;
	}
	public String getMovieReleaseCountry() {
		return movieReleaseCountry;
	}
	public void setMovieId(Long movieId) {
		this.movieId = movieId;
	}
	public void setMovieTitle(String movieTitle) {
		this.movieTitle = movieTitle;
	}
	public void setMovieYear(int movieYear) {
		this.movieYear = movieYear;
	}
	public void setMovieTime(int movieTime) {
		this.movieTime = movieTime;
	}
	public void setMovieLanguage(String movieLanguage) {
		this.movieLanguage = movieLanguage;
	}
	public void setMovieDateRelease(Date movieDateRelease) {
		this.movieDateRelease = movieDateRelease;
	}
	public void setMovieReleaseCountry(String movieReleaseCountry) {
		this.movieReleaseCountry = movieReleaseCountry;
	}

}
