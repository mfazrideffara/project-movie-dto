package com.example.movieproject.model.dto;

import java.io.Serializable;

public class MovieGenreDto implements Serializable{

	private static final long serialVersionUID = 1L;
	private MovieDto movie;
	private GenreDto genre;
	
	public MovieGenreDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	public MovieGenreDto(MovieDto movie, GenreDto genre) {
		super();
		this.movie = movie;
		this.genre = genre;
	}
	public MovieDto getMovie() {
		return movie;
	}
	public GenreDto getGenre() {
		return genre;
	}
	public void setMovie(MovieDto movie) {
		this.movie = movie;
	}
	public void setGenre(GenreDto genre) {
		this.genre = genre;
	}
	
}
