package com.example.movieproject.model.dto;

import java.io.Serializable;

public class DirectorDto implements Serializable{

	private static final long serialVersionUID = 1L;
	private Long directorId;
	private String directorFirstName;
	private String directorLastName;
	
	public Long getDirectorId() {
		return directorId;
	}
	public String getDirectorFirstName() {
		return directorFirstName;
	}
	public String getDirectorLastName() {
		return directorLastName;
	}
	public void setDirectorId(Long directorId) {
		this.directorId = directorId;
	}
	public void setDirectorFirstName(String directorFirstName) {
		this.directorFirstName = directorFirstName;
	}
	public void setDirectorLastName(String directorLastName) {
		this.directorLastName = directorLastName;
	}
	public DirectorDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	public DirectorDto(Long directorId, String directorFirstName, String directorLastName) {
		super();
		this.directorId = directorId;
		this.directorFirstName = directorFirstName;
		this.directorLastName = directorLastName;
	}
	
}
