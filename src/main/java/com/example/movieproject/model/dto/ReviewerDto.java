package com.example.movieproject.model.dto;

import java.io.Serializable;

public class ReviewerDto implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private Long reviewerId;
	private String reviewerName;
	
	public ReviewerDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ReviewerDto(Long reviewerId, String reviewerName) {
		super();
		this.reviewerId = reviewerId;
		this.reviewerName = reviewerName;
	}
	public Long getReviewerId() {
		return reviewerId;
	}
	public String getReviewerName() {
		return reviewerName;
	}
	public void setReviewerId(Long reviewerId) {
		this.reviewerId = reviewerId;
	}
	public void setReviewerName(String reviewerName) {
		this.reviewerName = reviewerName;
	}
	
	
}
