package com.example.movieproject.model.dto;

import java.io.Serializable;

import com.example.movieproject.model.Actor;

public class ActorDto implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private Long actorId;
	private String actorFirstName;
	private String actorLastName;
	private String gender;
	
	public static ActorDto valueOf(Actor actor) {
		ActorDto actorDto = new ActorDto();
		actorDto.setActorId(actor.getActorId());
		actorDto.setActorFirstName(actor.getActorFirstName());
		actorDto.setActorLastName(actor.getActorLastName());
		actorDto.setGender(actor.getGender());
		
        return actorDto;
	}
	
	public ActorDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public ActorDto(Long actorId, String actorFirstName, String actorLastName, String gender) {
		super();
		this.actorId = actorId;
		this.actorFirstName = actorFirstName;
		this.actorLastName = actorLastName;
		this.gender = gender;
	}
	
	public Long getActorId() {
		return actorId;
	}
	public String getActorFirstName() {
		return actorFirstName;
	}
	public String getActorLastName() {
		return actorLastName;
	}
	public String getGender() {
		return gender;
	}
	public void setActorId(Long actorId) {
		this.actorId = actorId;
	}
	public void setActorFirstName(String actorFirstName) {
		this.actorFirstName = actorFirstName;
	}
	public void setActorLastName(String actorLastName) {
		this.actorLastName = actorLastName;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
}
