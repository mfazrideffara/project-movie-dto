package com.example.movieproject.model;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name = "rating")
public class Rating implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	RatingKey id;
	
	@MapsId("rev_id")
	@ManyToOne
	@JoinColumn(name = "rev_id")
	private Reviewer reviewer;

	@MapsId("mov_id")
	@ManyToOne
	@JoinColumn(name = "mov_id")
	private Movie movie;
	 
	@Column(name = "rev_stars")
	private int reviewerStars;
	 
	@Column(name = "num_of_ratings")
	private int numberOfRatings;
	
	public Rating() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Rating(Reviewer reviewer, Movie movie, int reviewerStars, int numberOfRatings) {
		super();
		this.reviewer = reviewer;
		this.movie = movie;
		this.reviewerStars = reviewerStars;
		this.numberOfRatings = numberOfRatings;
	}

	public RatingKey getId() {
		return id;
	}

	public Reviewer getReviewer() {
		return reviewer;
	}

	public Movie getMovie() {
		return movie;
	}

	public int getReviewerStars() {
		return reviewerStars;
	}

	public int getNumberOfRatings() {
		return numberOfRatings;
	}

	public void setId(RatingKey id) {
		this.id = id;
	}

	public void setReviewer(Reviewer reviewer) {
		this.reviewer = reviewer;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public void setReviewerStars(int reviewerStars) {
		this.reviewerStars = reviewerStars;
	}

	public void setNumberOfRatings(int numberOfRatings) {
		this.numberOfRatings = numberOfRatings;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Rating)) return false;
        Rating that = (Rating) o;
        return Objects.equals(reviewer.getReviewerId(), that.reviewer.getReviewerId()) &&
        		Objects.equals(movie.getMovieId(), that.movie.getMovieId()) &&
        		Objects.equals(reviewerStars, that.reviewerStars) &&
                Objects.equals(numberOfRatings, that.numberOfRatings);
    }

	    @Override
	    public int hashCode() {
	        return Objects.hash(reviewer.getReviewerId(), movie.getMovieId(), reviewerStars, numberOfRatings);
	    }
}
