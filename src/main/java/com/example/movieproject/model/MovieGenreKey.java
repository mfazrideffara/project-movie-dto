package com.example.movieproject.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MovieGenreKey implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Column(name = "gen_id")
	Long genreId;

	@Column(name = "mov_id")
	Long movieId;

	public MovieGenreKey() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MovieGenreKey(Long genreId, Long movieId) {
		super();
		this.genreId = genreId;
		this.movieId = movieId;
	}
	
}

