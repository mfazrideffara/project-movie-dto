package com.example.movieproject.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Actor implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="act_id")
    private Long actorId;
	
	@Column(name="act_fname")
    private String actorFirstName;
    
	@Column(name="act_lname")
    private String actorLastName;
    
	@Column(name="act_gender")
    private String gender;
	
	@OneToMany(mappedBy = "actor", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<MovieCast> movieCasts = new HashSet<>();
	
	public Actor() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Actor(Long actorId, String actorFirstName, String actorLastName, String gender) {
		super();
		this.actorId = actorId;
		this.actorFirstName = actorFirstName;
		this.actorLastName = actorLastName;
		this.gender = gender;
	}

	public Long getActorId() {
		return actorId;
	}

	public String getActorFirstName() {
		return actorFirstName;
	}

	public String getActorLastName() {
		return actorLastName;
	}

	public String getGender() {
		return gender;
	}

	public Set<MovieCast> getMovies() {
		return movieCasts;
	}

	public void setActorId(Long actorId) {
		this.actorId = actorId;
	}

	public void setActorFirstName(String actorFirstName) {
		this.actorFirstName = actorFirstName;
	}

	public void setActorLastName(String actorLastName) {
		this.actorLastName = actorLastName;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public void setMovies(Set<MovieCast> movieCasts) {
		this.movieCasts = movieCasts;
	}
}
