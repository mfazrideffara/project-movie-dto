package com.example.movieproject.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Reviewer")
public class Reviewer implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="rev_id")
    private Long reviewerId;
	
	@Column(name="rev_name")
    private String reviewerName;
    
	@OneToMany(mappedBy = "reviewer", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<Rating> ratings = new HashSet<>();
	
	public Reviewer() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Reviewer(Long reviewerId, String reviewerName) {
		super();
		this.reviewerId = reviewerId;
		this.reviewerName = reviewerName;
	}

	public Long getReviewerId() {
		return reviewerId;
	}

	public String getReviewerName() {
		return reviewerName;
	}

	public Set<Rating> getRatings() {
		return ratings;
	}

	public void setReviewerId(Long reviewerId) {
		this.reviewerId = reviewerId;
	}

	public void setReviewerName(String reviewerName) {
		this.reviewerName = reviewerName;
	}

	public void setRatings(Set<Rating> ratings) {
		this.ratings = ratings;
	}
	
}
