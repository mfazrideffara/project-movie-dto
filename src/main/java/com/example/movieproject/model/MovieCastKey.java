package com.example.movieproject.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MovieCastKey implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Column(name = "act_id")
	Long actorId;

	@Column(name = "mov_id")
	Long movieId;

	public MovieCastKey() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MovieCastKey(Long actorId, Long movieId) {
		super();
		this.actorId = actorId;
		this.movieId = movieId;
	}

}
