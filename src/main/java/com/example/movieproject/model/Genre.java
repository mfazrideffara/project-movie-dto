package com.example.movieproject.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "genres")
public class Genre implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="gen_id")
    private Long genreId;
	
	@Column(name="gen_title")
    private String genreTitle;
    
	@OneToMany(mappedBy = "genre", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<MovieGenre> movieGenres = new HashSet<>();

	public Genre() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Genre(Long genreId, String genreTitle) {
		super();
		this.genreId = genreId;
		this.genreTitle = genreTitle;
	}

	public Long getGenreId() {
		return genreId;
	}

	public String getGenreTitle() {
		return genreTitle;
	}

	public Set<MovieGenre> getMovieGenres() {
		return movieGenres;
	}

	public void setGenreId(Long genreId) {
		this.genreId = genreId;
	}

	public void setGenreTitle(String genreTitle) {
		this.genreTitle = genreTitle;
	}

	public void setMovieGenres(Set<MovieGenre> movieGenres) {
		this.movieGenres = movieGenres;
	}
	
}