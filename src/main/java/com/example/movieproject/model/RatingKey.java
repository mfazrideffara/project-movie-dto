package com.example.movieproject.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class RatingKey implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Column(name = "rev_id")
	Long reviewerId;

	@Column(name = "mov_id")
	Long movieId;

	public RatingKey() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RatingKey(Long reviewerId, Long movieId) {
		super();
		this.reviewerId = reviewerId;
		this.movieId = movieId;
	}
}
