package com.example.movieproject.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Director implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="dir_id")
    private Long directorId;
	
	@Column(name="dir_fname")
    private String directorFirstName;
    
	@Column(name="dir_lname")
    private String directorLastName;
	
	@OneToMany(mappedBy = "director", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<MovieDirection> movieDirection = new HashSet<>();
	
	public Director() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Director(Long directorId, String directorFirstName, String directorLastName) {
		super();
		this.directorId = directorId;
		this.directorFirstName = directorFirstName;
		this.directorLastName = directorLastName;
	}

	public Long getDirectorId() {
		return directorId;
	}

	public String getDirectorFirstName() {
		return directorFirstName;
	}

	public String getDirectorLastName() {
		return directorLastName;
	}

	public Set<MovieDirection> getMovieDirection() {
		return movieDirection;
	}

	public void setDirectorId(Long directorId) {
		this.directorId = directorId;
	}

	public void setDirectorFirstName(String directorFirstName) {
		this.directorFirstName = directorFirstName;
	}

	public void setDirectorLastName(String directorLastName) {
		this.directorLastName = directorLastName;
	}

	public void setMovieDirection(Set<MovieDirection> movieDirection) {
		this.movieDirection = movieDirection;
	}
}
