package com.example.movieproject.model;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name = "movie_genres")
public class MovieGenre implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	MovieDirectionKey id;
	
	@MapsId("gen_id")
	@ManyToOne
	@JoinColumn(name = "gen_id")
	private Genre genre;

	@MapsId("mov_id")
	@ManyToOne
	@JoinColumn(name = "mov_id")
	private Movie movie;
	
	public MovieGenre() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MovieGenre(Genre genre, Movie movie) {
		super();
		this.genre = genre;
		this.movie = movie;
	}

	public MovieDirectionKey getId() {
		return id;
	}

	public Genre getGenre() {
		return genre;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setId(MovieDirectionKey id) {
		this.id = id;
	}

	public void setGenre(Genre genre) {
		this.genre = genre;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MovieGenre)) return false;
        MovieGenre that = (MovieGenre) o;
		return Objects.equals(movie.getMovieId(), that.movie.getMovieId()) &&
                Objects.equals(genre.getGenreId(), that.genre.getGenreId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(movie.getMovieId(), genre.getGenreId());
    }
}
