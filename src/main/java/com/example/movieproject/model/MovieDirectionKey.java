package com.example.movieproject.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MovieDirectionKey implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Column(name = "dir_id")
	Long directorId;

	@Column(name = "mov_id")
	Long movieId;

	public MovieDirectionKey() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MovieDirectionKey(Long directorId, Long movieId) {
		super();
		this.directorId = directorId;
		this.movieId = movieId;
	}
	
	
}
