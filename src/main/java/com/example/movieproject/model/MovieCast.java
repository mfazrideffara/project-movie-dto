package com.example.movieproject.model;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name = "movie_cast")
public class MovieCast implements Serializable {
	
	private static final long serialVersionUID = 1L;
		
	@Id	
	@Column(columnDefinition = "BINARY(16)")	
	private UUID id;	
	
	@MapsId("act_id")
	@ManyToOne
	@JoinColumn(name = "act_id")
	private Actor actor;

	@MapsId("mov_id")
	@ManyToOne
	@JoinColumn(name = "mov_id")
	private Movie movie;
	 
	private String role;

	public MovieCast(Actor actor, Movie movie, String role) {
		super();
		this.actor = actor;
		this.movie = movie;
		this.role = role;
	}

	public MovieCast() {
		
	}
	
	public Actor getActor() {
		return actor;
	}

	public Movie getMovie() {
		return movie;
	}

	public String getRole() {
		return role;
	}

	public void setActor(Actor actor) {
		this.actor = actor;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MovieCast)) return false;
        MovieCast that = (MovieCast) o;
		return Objects.equals(movie.getMovieId(), that.movie.getMovieId()) &&
                Objects.equals(actor.getActorId(), that.actor.getActorId()) &&
                Objects.equals(role, that.role);
    }

    @Override
    public int hashCode() {
        return Objects.hash(movie.getMovieId(), actor.getActorId(), role);
    }
}
