package com.example.movieproject.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.movieproject.model.Genre;
import com.example.movieproject.model.dto.GenreDto;
import com.example.movieproject.repository.GenreRepository;

@RestController
@RequestMapping("/api")
public class GenreController {
	
	 @Autowired
	 GenreRepository genreRepository;
	 
	 //Get All Genre
	 @GetMapping("/genre/readAll")
	 public HashMap<String, Object> getAllGenre() {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		List<GenreDto> listGenres = new ArrayList<GenreDto>();
		for(Genre g : genreRepository.findAll()) {
			GenreDto genreDto = new GenreDto(g.getGenreId(),
											 g.getGenreTitle());
			listGenres.add(genreDto);
		}
		
		String message;
        if(listGenres.isEmpty()) {
    		message = "Read All Failed!";
    	} else {
    		message = "Read All Success!";
    	}
    	showHashMap.put("Message", message);
    	showHashMap.put("Total", listGenres.size());
    	showHashMap.put("Data", listGenres);
		
		return showHashMap;
	 }
	 
	 // Read Genre By ID
	 @GetMapping("/genre/{id}")
	 public HashMap<String, Object> getById(@PathVariable(value = "id") Long id){
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		Genre genre = genreRepository.findById(id)
				.orElse(null);
		GenreDto genreDto = new GenreDto(genre.getGenreId(), 
										 genre.getGenreTitle());
		showHashMap.put("Messages", "Read Data Success");
		showHashMap.put("Data", genreDto);
		return showHashMap;
	}
	 
	// Create a new Genre
	@PostMapping("/genre/add")
	public HashMap<String, Object> createGenre(@Valid @RequestBody ArrayList<GenreDto> genreDto) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	@Valid ArrayList<GenreDto> listGenres = genreDto;
    	String message;
    	
    	for(GenreDto g : listGenres) {
    		Genre genre = new Genre(g.getGenreId(),
    							    g.getGenreTitle());
    		genreRepository.save(genre);
    	}
    
    	if(listGenres == null) {
    		message = "Create Failed!";
    	} else {
    		message = "Create Success!";
    	}
    	
    	showHashMap.put("Message", message);
    	showHashMap.put("Total Insert", listGenres.size());
    	showHashMap.put("Data", listGenres);
    	
    	return showHashMap;
    }
	
	// Update a Genre
    @PutMapping("/genre/update/{id}")
    public HashMap<String, Object> updateGenre(@PathVariable(value = "id") Long id,
            @Valid @RequestBody GenreDto genreDetails) {
    	
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	String message;
    	
    	int genreId = id.intValue();
    	List<Genre> listGenres = genreRepository.findAll();
    	
    	for(Genre g : listGenres) {
    		if(g.getGenreId() == genreId) {
    			if(genreDetails.getGenreTitle() == null) {
    				genreDetails.setGenreTitle(listGenres.get(genreId).getGenreTitle());
    	    	}
    		}
    	}	
    	
    	Genre genre = genreRepository.findById(id)
    			 .orElse(null);

    	genre.setGenreTitle(genreDetails.getGenreTitle());
    	
    	Genre updateGenre = genreRepository.save(genre);
    	
    	List<Genre> resultList = new ArrayList<Genre>();
    	resultList.add(updateGenre);
    	
    	if(resultList.isEmpty()) {
    		message = "Update Failed!";
    	} else {
    		message = "Update Success!";
    	}
    	
    	showHashMap.put("Message", message);
    	showHashMap.put("Total Update", resultList.size());
    	showHashMap.put("Data", resultList);
    	
    	return showHashMap;
    }
    
    // Delete a Genre
    @DeleteMapping("/genre/delete/{id}")
    public HashMap<String, Object> delete(@PathVariable(value = "id") Long id) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	Genre genre = genreRepository.findById(id)
    			.orElse(null);

    	genreRepository.delete(genre);

        showHashMap.put("Messages", "Delete Data Success!");
        showHashMap.put("Delete data :", genre);
    	return showHashMap;
    }
}
	 
