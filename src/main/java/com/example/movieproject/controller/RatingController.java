package com.example.movieproject.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.movieproject.model.Rating;
import com.example.movieproject.model.dto.RatingDto;
import com.example.movieproject.repository.RatingRepository;

@RestController
@RequestMapping("/api")
public class RatingController {
	
     ModelMapper modelMapper = new ModelMapper();
	
	 @Autowired
	 RatingRepository ratingRepository;
	
	 //GetAll Rating
	 @GetMapping("/rating/readAll")
	    public HashMap<String, Object> getAllRating() {
	        HashMap<String, Object> showHashMap = new HashMap<String, Object>();
	        ArrayList<RatingDto> ratingList = new ArrayList<RatingDto>();

	        for (Rating r : ratingRepository.findAll()) {
	        	ratingList.add(convertToDTO(r));
	        }

	        String message;
	        if(ratingList.isEmpty()) {
	    		message = "Read All Failed!";
	    	} else {
	    		message = "Read All Success!";
	    	}
	    	showHashMap.put("Message", message);
	    	showHashMap.put("Total", ratingList.size());
	    	showHashMap.put("Data", ratingList);
			
			return showHashMap;

	    }

	    public RatingDto convertToDTO(Rating rating) {
	    	RatingDto ratingDto = modelMapper.map(rating, RatingDto.class);
	        return ratingDto;
	    }
	    
	    private Rating convertToEntity(RatingDto ratingDto) {
	    	Rating rating = modelMapper.map(ratingDto, Rating.class);
	        return rating;
	    }
	 
	 // Read Rating By ID
	 @GetMapping("/rating/{id}")
	 public HashMap<String, Object> getById(@PathVariable(value = "id") Long id){
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		Rating rating = ratingRepository.findById(id)
				.orElse(null);
		RatingDto ratingDto = convertToDTO(rating);
		showHashMap.put("Messages", "Read Data Success");
		showHashMap.put("Data", ratingDto);
		return showHashMap;
	}
	 
	// Create a new Rating
	@PostMapping("/rating/add")
	public HashMap<String, Object> createRating(@Valid @RequestBody ArrayList<RatingDto> RatingDto) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	@Valid ArrayList<RatingDto> listRatings = RatingDto;
    	String message;
    
    	for(RatingDto r : listRatings) {
    		Rating rating = convertToEntity(r);
    		ratingRepository.save(rating);
    	}
    
    	if(listRatings == null) {
    		message = "Create Failed!";
    	} else {
    		message = "Create Success!";
    	}
    	
    	showHashMap.put("Message", message);
    	showHashMap.put("Total Insert", listRatings.size());
    	showHashMap.put("Data", listRatings);
    	
    	return showHashMap;
    }
	
	// Update a Rating
    @PutMapping("/rating/update/{id}")
    public HashMap<String, Object> updateRating(@PathVariable(value = "id") Long id,
            @Valid @RequestBody RatingDto ratingDetails) {
    	
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	String message;
    	
    	int ratingId = id.intValue();
    	List<Rating> listRatings = ratingRepository.findAll();
    	
    	
		if(ratingDetails.getReviewer() == null) {
			listRatings.get(ratingId).setReviewer(listRatings.get(ratingId).getReviewer());
    	}
		if(ratingDetails.getMovie() == null) {
			listRatings.get(ratingId).setMovie(listRatings.get(ratingId).getMovie());
    	}
		if(ratingDetails.getReviewerStars() == 0) {
			listRatings.get(ratingId).setReviewerStars(listRatings.get(ratingId).getReviewerStars());
    	}
		if(ratingDetails.getNumberOfRatings() == 0) {
			listRatings.get(ratingId).setNumberOfRatings(listRatings.get(ratingId).getNumberOfRatings());
    	}
    		
    	Rating rating = ratingRepository.findById(id)
    			 .orElse(null);

    	rating.setReviewer(convertToEntity(ratingDetails).getReviewer());
    	rating.setMovie(convertToEntity(ratingDetails).getMovie());
    	rating.setReviewerStars(convertToEntity(ratingDetails).getReviewerStars());
    	rating.setNumberOfRatings(convertToEntity(ratingDetails).getNumberOfRatings());
    	
    	Rating updateRating = ratingRepository.save(rating);
    	
    	List<Rating> resultList = new ArrayList<Rating>();
    	resultList.add(updateRating);
    	
    	if(resultList.isEmpty()) {
    		message = "Update Failed!";
    	} else {
    		message = "Update Success!";
    	}
    	
    	showHashMap.put("Message", message);
    	showHashMap.put("Total Update", resultList.size());
    	showHashMap.put("Data", resultList);
    	
    	return showHashMap;
    }
    
    // Delete a Rating
    @DeleteMapping("/rating/delete/{id}")
    public HashMap<String, Object> delete(@PathVariable(value = "id") Long id) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	Rating rating = ratingRepository.findById(id)
    			.orElse(null);

    	ratingRepository.delete(rating);

        showHashMap.put("Messages", "Delete Data Success!");
        showHashMap.put("Delete data :", rating);
    	return showHashMap;
    }
}
	 
