package com.example.movieproject.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.movieproject.model.Actor;
import com.example.movieproject.model.dto.ActorDto;
import com.example.movieproject.repository.ActorRepository;

@RestController
@RequestMapping("/api")
public class ActorController {
	
	 @Autowired
	 ActorRepository actorRepository;
	 
	 //Get All Actor
	 @GetMapping("/actor/readAll")
	 public HashMap<String, Object> getAllActor() {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		List<ActorDto> listActors = new ArrayList<ActorDto>();
		for(Actor a : actorRepository.findAll()) {
			ActorDto actorDto = new ActorDto(a.getActorId(),
											 a.getActorFirstName(),
											 a.getActorLastName(),
											 a.getGender());
			listActors.add(actorDto);
		}
		
		String message;
        if(listActors.isEmpty()) {
    		message = "Read All Failed!";
    	} else {
    		message = "Read All Success!";
    	}
    	showHashMap.put("Message", message);
    	showHashMap.put("Total", listActors.size());
    	showHashMap.put("Data", listActors);
		
		return showHashMap;
	 }
	 
	 // Read Actor By ID
	 @GetMapping("/actor/{id}")
	 public HashMap<String, Object> getById(@PathVariable(value = "id") Long id){
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		Actor actor = actorRepository.findById(id)
				.orElse(null);
		ActorDto actorDto = new ActorDto(actor.getActorId(), 
										 actor.getActorFirstName(), 
										 actor.getActorLastName(),
										 actor.getGender());
		showHashMap.put("Messages", "Read Data Success");
		showHashMap.put("Data", actorDto);
		return showHashMap;
		}
	 
	// Create a new Actor
	@PostMapping("/actor/add")
	public HashMap<String, Object> createActor(@Valid @RequestBody ArrayList<ActorDto> actorDto) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	@Valid ArrayList<ActorDto> listActors = actorDto;
    	String message;
    	
    	for(ActorDto a : listActors) {
    		Actor actor = new Actor(a.getActorId(),
								    a.getActorFirstName(),
								    a.getActorLastName(),
								    a.getGender());
    		actorRepository.save(actor);
    	}
    
    	if(listActors == null) {
    		message = "Create Failed!";
    	} else {
    		message = "Create Success!";
    	}
    	
    	showHashMap.put("Message", message);
    	showHashMap.put("Total Insert", listActors.size());
    	showHashMap.put("Data", listActors);
    	
    	return showHashMap;
    }
	
	// Update a Actor
    @PutMapping("/actor/update/{id}")
    public HashMap<String, Object> updateActor(@PathVariable(value = "id") Long id,
            @Valid @RequestBody ActorDto actorDetails) {
    	
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	String message;
    	
    	int actorId = id.intValue();
    	List<Actor> listActors = actorRepository.findAll();
    	
    	for(Actor a : listActors) {
    		if(a.getActorId() == actorId) {
    			if(actorDetails.getActorFirstName() == null) {
    				actorDetails.setActorFirstName(listActors.get(actorId).getActorFirstName());
    	    	}
    	    	if(actorDetails.getActorLastName() == null) {
    	    		actorDetails.setActorLastName(listActors.get(actorId).getActorLastName());
    	    	}
    	    	if(actorDetails.getGender() == null) {
    	    		actorDetails.setGender(listActors.get(actorId).getActorLastName());
    	    	}
    		}
    	}	
    	
    	Actor actor = actorRepository.findById(id)
    			 .orElse(null);

    	actor.setActorFirstName(actorDetails.getActorFirstName());
    	actor.setActorLastName(actorDetails.getActorLastName());
    	actor.setGender(actorDetails.getGender());
    	
    	Actor updateActor = actorRepository.save(actor);
    	
    	List<Actor> resultList = new ArrayList<Actor>();
    	resultList.add(updateActor);
    	
    	if(resultList.isEmpty()) {
    		message = "Update Failed!";
    	} else {
    		message = "Update Success!";
    	}
    	
    	showHashMap.put("Message", message);
    	showHashMap.put("Total Update", resultList.size());
    	showHashMap.put("Data", resultList);
    	
    	return showHashMap;
    }
    
    // Delete a Actor
    @DeleteMapping("/actor/delete/{id}")
    public HashMap<String, Object> delete(@PathVariable(value = "id") Long id) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	Actor actor = actorRepository.findById(id)
    			.orElse(null);

        actorRepository.delete(actor);

        showHashMap.put("Messages", "Delete Data Success!");
        showHashMap.put("Delete data :", actor);
    	return showHashMap;
    }
}
	 
