package com.example.movieproject.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.movieproject.model.MovieGenre;
import com.example.movieproject.model.dto.MovieGenreDto;
import com.example.movieproject.repository.MovieGenreRepository;

@RestController
@RequestMapping("/api")
public class MovieGenreController {
	
     ModelMapper modelMapper = new ModelMapper();
	
	 @Autowired
	 MovieGenreRepository movieGenreRepository;
	
	 //GetAll MovieGenre
	 @GetMapping("/movieGenre/readAll")
	    public HashMap<String, Object> getAllMovieGenre() {
	        HashMap<String, Object> showHashMap = new HashMap<String, Object>();
	        ArrayList<MovieGenreDto> movieGenreList = new ArrayList<MovieGenreDto>();

	        for (MovieGenre mc : movieGenreRepository.findAll()) {
	        	movieGenreList.add(convertToDTO(mc));
	        }

	        String message;
	        if(movieGenreList.isEmpty()) {
	    		message = "Read All Failed!";
	    	} else {
	    		message = "Read All Success!";
	    	}
	    	showHashMap.put("Message", message);
	    	showHashMap.put("Total", movieGenreList.size());
	    	showHashMap.put("Data", movieGenreList);
			
			return showHashMap;

	    }

	    public MovieGenreDto convertToDTO(MovieGenre movieGenre) {
	    	MovieGenreDto movieGenreDto = modelMapper.map(movieGenre, MovieGenreDto.class);
	        return movieGenreDto;
	    }
	    
	    private MovieGenre convertToEntity(MovieGenreDto movieGenreDto) {
	    	MovieGenre movieGenre = modelMapper.map(movieGenreDto, MovieGenre.class);
	        return movieGenre;
	    }
	 
	 // Read MovieGenre By ID
	 @GetMapping("/movieGenre/{id}")
	 public HashMap<String, Object> getById(@PathVariable(value = "id") Long id){
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		MovieGenre movieGenre = movieGenreRepository.findById(id)
				.orElse(null);
		MovieGenreDto movieGenreDto = convertToDTO(movieGenre);
		showHashMap.put("Messages", "Read Data Success");
		showHashMap.put("Data", movieGenreDto);
		return showHashMap;
	}
	 
	// Create a new MovieGenre
	@PostMapping("/movieGenre/add")
	public HashMap<String, Object> createMovieGenre(@Valid @RequestBody ArrayList<MovieGenreDto> MovieGenreDto) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	@Valid ArrayList<MovieGenreDto> listMovieGenres = MovieGenreDto;
    	String message;
    
    	for(MovieGenreDto r : listMovieGenres) {
    		MovieGenre movieGenre = convertToEntity(r);
    		movieGenreRepository.save(movieGenre);
    	}
    
    	if(listMovieGenres == null) {
    		message = "Create Failed!";
    	} else {
    		message = "Create Success!";
    	}
    	
    	showHashMap.put("Message", message);
    	showHashMap.put("Total Insert", listMovieGenres.size());
    	showHashMap.put("Data", listMovieGenres);
    	
    	return showHashMap;
    }
	
	// Update a MovieGenre
    @PutMapping("/MovieGenre/update/{id}")
    public HashMap<String, Object> updateMovieGenre(@PathVariable(value = "id") Long id,
            @Valid @RequestBody MovieGenreDto movieGenreDetails) {
    	
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	String message;
    	
    	int movieGenreId = id.intValue();
    	List<MovieGenre> listMovieGenres = movieGenreRepository.findAll();
    	
    	
		if(movieGenreDetails.getGenre() == null) {
			listMovieGenres.get(movieGenreId).setGenre(listMovieGenres.get(movieGenreId).getGenre());
    	}
		if(movieGenreDetails.getMovie() == null) {
			listMovieGenres.get(movieGenreId).setMovie(listMovieGenres.get(movieGenreId).getMovie());
    	}
    		
    	MovieGenre movieGenre = movieGenreRepository.findById(id)
    			 .orElse(null);

    	movieGenre.setGenre(convertToEntity(movieGenreDetails).getGenre());
    	movieGenre.setMovie(convertToEntity(movieGenreDetails).getMovie());
    	
    	MovieGenre updateMovieGenre = movieGenreRepository.save(movieGenre);
    	
    	List<MovieGenre> resultList = new ArrayList<MovieGenre>();
    	resultList.add(updateMovieGenre);
    	
    	if(resultList.isEmpty()) {
    		message = "Update Failed!";
    	} else {
    		message = "Update Success!";
    	}
    	
    	showHashMap.put("Message", message);
    	showHashMap.put("Total Update", resultList.size());
    	showHashMap.put("Data", resultList);
    	
    	return showHashMap;
    }
    
    // Delete a MovieGenre
    @DeleteMapping("/MovieGenre/delete/{id}")
    public HashMap<String, Object> delete(@PathVariable(value = "id") Long id) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	MovieGenre movieGenre = movieGenreRepository.findById(id)
    			.orElse(null);

    	movieGenreRepository.delete(movieGenre);

        showHashMap.put("Messages", "Delete Data Success!");
        showHashMap.put("Delete data :", movieGenre);
    	return showHashMap;
    }
}
	 
