package com.example.movieproject.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.movieproject.model.MovieDirection;
import com.example.movieproject.model.dto.MovieDirectionDto;
import com.example.movieproject.repository.MovieDirectionRepository;

@RestController
@RequestMapping("/api")
public class MovieDirectionController {
	
     ModelMapper modelMapper = new ModelMapper();
	
	 @Autowired
	 MovieDirectionRepository movieDirectionRepository;
	
	 //GetAll MovieDirection
	 @GetMapping("/movieDirection/readAll")
	    public HashMap<String, Object> getAllMovieDirection() {
	        HashMap<String, Object> showHashMap = new HashMap<String, Object>();
	        ArrayList<MovieDirectionDto> movieDirectionList = new ArrayList<MovieDirectionDto>();

	        for (MovieDirection mc : movieDirectionRepository.findAll()) {
	        	movieDirectionList.add(convertToDTO(mc));
	        }

	        String message;
	        if(movieDirectionList.isEmpty()) {
	    		message = "Read All Failed!";
	    	} else {
	    		message = "Read All Success!";
	    	}
	    	showHashMap.put("Message", message);
	    	showHashMap.put("Total", movieDirectionList.size());
	    	showHashMap.put("Data", movieDirectionList);
			
			return showHashMap;
	    }

	    public MovieDirectionDto convertToDTO(MovieDirection movieDirection) {
	    	MovieDirectionDto movieDirectionDto = modelMapper.map(movieDirection, MovieDirectionDto.class);
	        return movieDirectionDto;
	    }
	    
	    private MovieDirection convertToEntity(MovieDirectionDto movieDirectionDto) {
	    	MovieDirection movieDirection = modelMapper.map(movieDirectionDto, MovieDirection.class);
	        return movieDirection;
	    }
	 
	 // Read MovieDirection By ID
	 @GetMapping("/movieDirection/{id}")
	 public HashMap<String, Object> getById(@PathVariable(value = "id") Long id){
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		MovieDirection movieDirection = movieDirectionRepository.findById(id)
				.orElse(null);
		MovieDirectionDto MovieDirectionDto = convertToDTO(movieDirection);
		showHashMap.put("Messages", "Read Data Success");
		showHashMap.put("Data", MovieDirectionDto);
		return showHashMap;
	}
	 
	// Create a new MovieDirection
	@PostMapping("/movieDirection/add")
	public HashMap<String, Object> createMovieDirection(@Valid @RequestBody ArrayList<MovieDirectionDto> movieDirectionDto) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	@Valid ArrayList<MovieDirectionDto> listMovieDirections = movieDirectionDto;
    	String message;
    
    	for(MovieDirectionDto mc : listMovieDirections) {
    		MovieDirection movieDirection = convertToEntity(mc);
    		movieDirectionRepository.save(movieDirection);
    	}
    
    	if(listMovieDirections == null) {
    		message = "Create Failed!";
    	} else {
    		message = "Create Success!";
    	}
    	
    	showHashMap.put("Message", message);
    	showHashMap.put("Total Insert", listMovieDirections.size());
    	showHashMap.put("Data", listMovieDirections);
    	
    	return showHashMap;
    }
	
	// Update a MovieDirection
    @PutMapping("/movieDirection/update/{id}")
    public HashMap<String, Object> updateMovieDirection(@PathVariable(value = "id") Long id,
            @Valid @RequestBody MovieDirectionDto MovieDirectionDetails) {
    	
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	String message;
    	
    	int movieDirectionId = id.intValue();
    	List<MovieDirection> listMovieDirections = movieDirectionRepository.findAll();
    	
    	
		if(MovieDirectionDetails.getDirector() == null) {
			listMovieDirections.get(movieDirectionId).setDirector(listMovieDirections.get(movieDirectionId).getDirector());
    	}
		if(MovieDirectionDetails.getMovie() == null) {
			listMovieDirections.get(movieDirectionId).setMovie(listMovieDirections.get(movieDirectionId).getMovie());
    	}
    		
    	MovieDirection movieDirection = movieDirectionRepository.findById(id)
    			 .orElse(null);

    	movieDirection.setDirector(convertToEntity(MovieDirectionDetails).getDirector());
    	movieDirection.setMovie(convertToEntity(MovieDirectionDetails).getMovie());
    	
    	MovieDirection updateMovieDirection = movieDirectionRepository.save(movieDirection);
    	
    	List<MovieDirection> resultList = new ArrayList<MovieDirection>();
    	resultList.add(updateMovieDirection);
    	
    	if(resultList.isEmpty()) {
    		message = "Update Failed!";
    	} else {
    		message = "Update Success!";
    	}
    	
    	showHashMap.put("Message", message);
    	showHashMap.put("Total Update", resultList.size());
    	showHashMap.put("Data", resultList);
    	
    	return showHashMap;
    }
    
    // Delete a MovieDirection
    @DeleteMapping("/movieDirection/delete/{id}")
    public HashMap<String, Object> delete(@PathVariable(value = "id") Long id) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	MovieDirection movieDirection = movieDirectionRepository.findById(id)
    			.orElse(null);

    	movieDirectionRepository.delete(movieDirection);

        showHashMap.put("Messages", "Delete Data Success!");
        showHashMap.put("Delete data :", movieDirection);
    	return showHashMap;
    }
}
	 
