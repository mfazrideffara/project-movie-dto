package com.example.movieproject.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.movieproject.model.Movie;
import com.example.movieproject.model.dto.MovieDto;
import com.example.movieproject.repository.MovieRepository;

@RestController
@RequestMapping("/api")
public class MovieController {
	
	 @Autowired
	 MovieRepository movieRepository;
	 
	 //Get All Movie
	 @GetMapping("/movie/readAll")
	 public HashMap<String, Object> getAllMovie() {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		List<MovieDto> listMovies = new ArrayList<MovieDto>();
		for(Movie m : movieRepository.findAll()) {
			MovieDto movieDto = new MovieDto(m.getMovieId(),
											 m.getMovieTitle(),
											 m.getMovieYear(),
											 m.getMovieTime(),
											 m.getMovieLanguage(),
											 m.getMovieDateRelease(),
											 m.getMovieReleaseCountry());
			listMovies.add(movieDto);
		}
		
		String message;
        if(listMovies.isEmpty()) {
    		message = "Read All Failed!";
    	} else {
    		message = "Read All Success!";
    	}
    	showHashMap.put("Message", message);
    	showHashMap.put("Total", listMovies.size());
    	showHashMap.put("Data", listMovies);
		
		return showHashMap;
	 }
	 
	 // Read Movie By ID
	 @GetMapping("/movie/{id}")
	 public HashMap<String, Object> getById(@PathVariable(value = "id") Long id){
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		Movie movie = movieRepository.findById(id)
				.orElse(null);
		MovieDto movieDto = new MovieDto(movie.getMovieId(),
										 movie.getMovieTitle(),
										 movie.getMovieYear(),
										 movie.getMovieTime(),
										 movie.getMovieLanguage(),
										 movie.getMovieDateRelease(),
										 movie.getMovieReleaseCountry());
		showHashMap.put("Messages", "Read Data Success");
		showHashMap.put("Data", movieDto);
		return showHashMap;
	}
	 
	// Create a new Movie
	@PostMapping("/movie/add")
	public HashMap<String, Object> createMovie(@Valid @RequestBody ArrayList<MovieDto> movieDto) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	@Valid ArrayList<MovieDto> listMovies = movieDto;
    	String message;
    	
    	for(MovieDto m : listMovies) {
    		Movie Movie = new Movie(m.getMovieId(),
									m.getMovieTitle(),
									m.getMovieYear(),
									m.getMovieTime(),
									m.getMovieLanguage(),
									m.getMovieDateRelease(),
									m.getMovieReleaseCountry());
    		movieRepository.save(Movie);
    	}
    
    	if(listMovies == null) {
    		message = "Create Failed!";
    	} else {
    		message = "Create Success!";
    	}
    	
    	showHashMap.put("Message", message);
    	showHashMap.put("Total Insert", listMovies.size());
    	showHashMap.put("Data", listMovies);
    	
    	return showHashMap;
    }
	
	// Update a Movie
    @PutMapping("/movie/update/{id}")
    public HashMap<String, Object> updateMovie(@PathVariable(value = "id") Long id,
            @Valid @RequestBody MovieDto movieDetails) {
    	
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	String message;
    	
    	int movieId = id.intValue();
    	List<Movie> listMovies = movieRepository.findAll();
    	
    	for(Movie g : listMovies) {
    		if(g.getMovieId() == movieId) {
    			if(movieDetails.getMovieTitle() == null) {
    				movieDetails.setMovieTitle(listMovies.get(movieId).getMovieTitle());
    	    	}
    			if(movieDetails.getMovieYear() == 0) {
    				movieDetails.setMovieYear(listMovies.get(movieId).getMovieYear());
    	    	}
    			if(movieDetails.getMovieTime() == 0) {
    				movieDetails.setMovieTime(listMovies.get(movieId).getMovieTime());
    	    	}
    			if(movieDetails.getMovieLanguage() == null) {
    				movieDetails.setMovieLanguage(listMovies.get(movieId).getMovieLanguage());
    	    	}
    			if(movieDetails.getMovieDateRelease() == null) {
    				movieDetails.setMovieDateRelease(listMovies.get(movieId).getMovieDateRelease());
    	    	}
    			if(movieDetails.getMovieReleaseCountry() == null) {
    				movieDetails.setMovieReleaseCountry(listMovies.get(movieId).getMovieReleaseCountry());
    	    	}
    		}
    	}	
    	
    	Movie movie = movieRepository.findById(id)
    			 .orElse(null);

    	movie.setMovieTitle(movieDetails.getMovieTitle());
    	movie.setMovieYear(movieDetails.getMovieYear());
    	movie.setMovieTime(movieDetails.getMovieTime());
    	movie.setMovieLanguage(movieDetails.getMovieLanguage());
    	movie.setMovieDateRelease(movieDetails.getMovieDateRelease());
    	movie.setMovieReleaseCountry(movieDetails.getMovieReleaseCountry());
    	
    	Movie updateMovie = movieRepository.save(movie);
    	
    	List<Movie> resultList = new ArrayList<Movie>();
    	resultList.add(updateMovie);
    	
    	if(resultList.isEmpty()) {
    		message = "Update Failed!";
    	} else {
    		message = "Update Success!";
    	}
    	
    	showHashMap.put("Message", message);
    	showHashMap.put("Total Update", resultList.size());
    	showHashMap.put("Data", resultList);
    	
    	return showHashMap;
    }
    
    // Delete a Movie
    @DeleteMapping("/movie/delete/{id}")
    public HashMap<String, Object> delete(@PathVariable(value = "id") Long id) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	Movie movie = movieRepository.findById(id)
    			.orElse(null);

    	movieRepository.delete(movie);

        showHashMap.put("Messages", "Delete Data Success!");
        showHashMap.put("Delete data :", movie);
    	return showHashMap;
    }
}
	 
