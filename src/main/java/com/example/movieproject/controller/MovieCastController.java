package com.example.movieproject.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.movieproject.model.MovieCast;
import com.example.movieproject.model.dto.MovieCastDto;
import com.example.movieproject.repository.MovieCastRepository;

@RestController
@RequestMapping("/api")
public class MovieCastController {
	
     ModelMapper modelMapper = new ModelMapper();
	
	 @Autowired
	 MovieCastRepository movieCastRepository;
	
	 //All Read MovieCast By ID
	 @GetMapping("/movieCast/readAll")
	    public HashMap<String, Object> getAllMovieCast() {
	        HashMap<String, Object> showHashMap = new HashMap<String, Object>();
	        ArrayList<MovieCastDto> movieCastList = new ArrayList<MovieCastDto>();
	
	        for (MovieCast mc : movieCastRepository.findAll()) {
	            movieCastList.add(convertToDTO(mc));
	        }
	

	        String message;
	        if(movieCastList.isEmpty()) {
	    		message = "Read All Failed!";
	    	} else {
	    		message = "Read All Success!";
	    	}
	    	showHashMap.put("Message", message);
	    	showHashMap.put("Total", movieCastList.size());
	    	showHashMap.put("Data", movieCastList);
			
			return showHashMap;
	    }

	    public MovieCastDto convertToDTO(MovieCast movieCast) {
	    	MovieCastDto movieCastDto = modelMapper.map(movieCast, MovieCastDto.class);
	        return movieCastDto;
	    }
	    
	    private MovieCast convertToEntity(MovieCastDto movieCastDto) {
	    	MovieCast movieCast = modelMapper.map(movieCastDto, MovieCast.class);
	        return movieCast;
	    }
	 
	 // Read MovieCast By ID
	 @GetMapping("/movieCast/{id}")
	 public HashMap<String, Object> getById(@PathVariable(value = "id") Long id){
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		MovieCast movieCast = movieCastRepository.findById(id)
				.orElse(null);
		MovieCastDto MovieCastDto = convertToDTO(movieCast);
		showHashMap.put("Messages", "Read Data Success");
		showHashMap.put("Data", MovieCastDto);
		return showHashMap;
	}
	 
	// Create a new MovieCast
	@PostMapping("/movieCast/add")
	public HashMap<String, Object> createMovieCast(@Valid @RequestBody ArrayList<MovieCastDto> movieCastDto) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	@Valid ArrayList<MovieCastDto> listMovieCasts = movieCastDto;
    	String message;
    
    	for(MovieCastDto mc : listMovieCasts) {
    		MovieCast movieCast = convertToEntity(mc);
    		movieCastRepository.save(movieCast);
    	}
    
    	if(listMovieCasts == null) {
    		message = "Create Failed!";
    	} else {
    		message = "Create Success!";
    	}
    	
    	showHashMap.put("Message", message);
    	showHashMap.put("Total Insert", listMovieCasts.size());
    	showHashMap.put("Data", listMovieCasts);
    	
    	return showHashMap;
    }
	
	// Update a MovieCast
    @PutMapping("/movieCast/update/{id}")
    public HashMap<String, Object> updateMovieCast(@PathVariable(value = "id") Long id,
            @Valid @RequestBody MovieCastDto movieCastDetails) {
    	
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	String message;
    	
    	int movieCastId = id.intValue();
    	List<MovieCast> listMovieCasts = movieCastRepository.findAll();
    	
    	
		if(movieCastDetails.getActor() == null) {
			listMovieCasts.get(movieCastId).setActor(listMovieCasts.get(movieCastId).getActor());
    	}
		if(movieCastDetails.getMovie() == null) {
			listMovieCasts.get(movieCastId).setMovie(listMovieCasts.get(movieCastId).getMovie());
    	}
    		
    	MovieCast movieCast = movieCastRepository.findById(id)
    			 .orElse(null);

    	movieCast.setActor(convertToEntity(movieCastDetails).getActor());
    	movieCast.setMovie(convertToEntity(movieCastDetails).getMovie());
    	
    	MovieCast updateMovieCast = movieCastRepository.save(movieCast);
    	
    	List<MovieCast> resultList = new ArrayList<MovieCast>();
    	resultList.add(updateMovieCast);
    	
    	if(resultList.isEmpty()) {
    		message = "Update Failed!";
    	} else {
    		message = "Update Success!";
    	}
    	
    	showHashMap.put("Message", message);
    	showHashMap.put("Total Update", resultList.size());
    	showHashMap.put("Data", resultList);
    	
    	return showHashMap;
    }
    
    // Delete a PaperQuality
    @DeleteMapping("/movieCast/delete/{id}")
    public HashMap<String, Object> delete(@PathVariable(value = "id") Long id) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	MovieCast movieCast = movieCastRepository.findById(id)
    			.orElse(null);

    	movieCastRepository.delete(movieCast);

        showHashMap.put("Messages", "Delete Data Success!");
        showHashMap.put("Delete data :", movieCast);
    	return showHashMap;
    }
}
	 
