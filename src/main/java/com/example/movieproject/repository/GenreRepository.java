package com.example.movieproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.movieproject.model.Genre;

public interface GenreRepository extends JpaRepository<Genre, Long>{

}
