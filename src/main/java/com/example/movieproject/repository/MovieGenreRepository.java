package com.example.movieproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.movieproject.model.MovieGenre;

public interface MovieGenreRepository extends JpaRepository<MovieGenre, Long>{

}	
