package com.example.movieproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.movieproject.model.Director;

public interface DirectorRepository extends JpaRepository<Director, Long>{

}
