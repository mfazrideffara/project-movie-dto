package com.example.movieproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.movieproject.model.Movie;

public interface MovieRepository extends JpaRepository<Movie, Long>{

}
