package com.example.movieproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.movieproject.model.Rating;

public interface RatingRepository extends JpaRepository<Rating, Long>{

}
