package com.example.movieproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.movieproject.model.MovieCast;

public interface MovieCastRepository extends JpaRepository<MovieCast, Long>{

}
