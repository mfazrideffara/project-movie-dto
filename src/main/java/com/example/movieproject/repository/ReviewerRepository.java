package com.example.movieproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.movieproject.model.Reviewer;

public interface ReviewerRepository extends JpaRepository<Reviewer, Long>{

}
