package com.example.movieproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.movieproject.model.Actor;

public interface ActorRepository extends JpaRepository<Actor, Long>{

}
