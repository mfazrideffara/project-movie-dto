package com.example.movieproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.movieproject.model.MovieDirection;

public interface MovieDirectionRepository extends JpaRepository<MovieDirection, Long>{

}
